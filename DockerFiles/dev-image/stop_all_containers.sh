#!/bin/sh
echo Stop NATS
docker stop nats-main
docker rm nats-main
echo Stopping Coordination service
docker stop coord-service
docker rm coord-service 
echo Stopping Devimage
docker stop devimage
docker rm devimage
