#!/bin/bash

# Update
sudo apt-get update

# Install Docker
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common git -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker 

# Get the Quartz repo
mkdir workspace
cd workspace
git clone https://bitbucket.org/sandeepdsouza93/quartz.git
cd quartz/DockerFiles/dev-image
/bin/bash build.sh

./run_priveleged_host.sh /home/sandeepdsouza93/workspace/quartz/
cd /opt/qot-stack; git submodule init; git submodule update; cd /opt/qot-stack/thirdparty/; cd cnats; mkdir build; cd build; cmake ..; make; make install; cd ../../..; mkdir build; cd build; ccmake ..

 docker build -t qot-coord-service -f coordination-service/Dockerfile .

./run_all_services.sh /home/sandeepdsouza93/workspace/quartz/ 40.71.102.215:2181 azure-1