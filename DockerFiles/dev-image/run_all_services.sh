#!/bin/sh
# The first argument is the absolute path to the repo and the second is the zookeeper server (40.71.102.215:2181)
# the third argument is the node name (azure-1)
echo Starting NATS
docker run -d --name nats-main -p 4222:4222 -p 6222:6222 -p 8222:8222 nats
echo Starting Coordination service
docker run -d --network host --privileged --name coord-service -p 8502:8502 qot-coord-service
docker exec -d coord-service python coordination_service/app.py -z $2 -c $3 
echo Running the QoT Dev Docker Container with priveleges
docker run -d --network host --privileged --name devimage -v $1:/opt/qot-stack:z qot-devimage /bin/sh -c "ping localhost"
docker exec devimage /bin/sh -c "cd /opt/qot-stack/thirdparty/cnats/build; make install; cd ../../../build; make install; ldconfig"
echo Starting the Timeline Service
docker exec -d devimage /bin/sh -c "qot_timeline_service"
echo Starting the Sync Service
docker exec -d devimage /bin/sh -c "qot_sync_service -v"
echo Waiting for the sync service to stabilize
sleep 60
echo Starting the Application
docker exec -it devimage /bin/sh -c "helloworld_core_cpp gl_my_test_timeline app 1000 1000000"
